import getImageData from "./getImageData";
import makeImageMatrix from "./makeImageMatrix";
import nearestNeighborAlg from "./nearestNeighborAlg";

export default function changeImageSize(image, widthScale, heightScale) {
	let imageData = getImageData(image);
	let imageMatrix = makeImageMatrix(imageData, image.width);
    let newImage = nearestNeighborAlg(imageMatrix, image.width, image.height, widthScale, heightScale);
	
	return newImage;
}
