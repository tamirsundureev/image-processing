import setMpAfterChange from "./setMpAfterChange";


let units = document.getElementsByClassName('dialog_units');

export default function writeUnitsOnChange(radio, oldWidth, oldHeight) {
	radio.onchange = function() {
		units[0].innerText = this.value;
		units[1].innerText = this.value;
		setMpAfterChange(oldWidth, oldHeight);
	}
}
