export default function nearestNeighborAlg(imageMatrix, imageWidth, imageHeight, widthScale, heightScale) {
	let height = Math.floor(imageHeight * heightScale);
	let width = Math.floor(imageWidth * widthScale);
	let buffer = new Uint8ClampedArray(width * height * 4);
	let pos = 0;

	for (let y2 = 1; y2 <= height; y2++) {
		let y = Math.ceil(y2 / heightScale);
		for (let x2 = 1; x2 <= width; x2++) {
			let x = Math.ceil(x2 / widthScale) * 4;
			buffer[pos  ] = imageMatrix[y][x - 3];
			buffer[pos+1] = imageMatrix[y][x - 2];
			buffer[pos+2] = imageMatrix[y][x - 1];
			buffer[pos+3] = imageMatrix[y][x];
			pos += 4;
		}
	}

	let canvasHelper = document.createElement('canvas');
    canvasHelper.width = width;
    canvasHelper.height = height;
        
    let canvasHelperCtx = canvasHelper.getContext('2d');
	let imageData = canvasHelperCtx.createImageData(width, height);

	imageData.data.set(buffer);
	
	return imageData;
}
