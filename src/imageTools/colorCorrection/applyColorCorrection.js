import setImageData from "../../setImageData";
import drawColorCorrectionLine from "./drawColorCorrectionLine";

let canvas = document.getElementById("canvas");
let ctx = canvas.getContext('2d');

let imageData;

export default function applyColorCorrection(isPreview = null) {
    let width = Math.floor(canvas.width);
    let height = Math.floor(canvas.height);
    let buffer = new Uint8ClampedArray(width * height * 4);

    let correctionData = drawColorCorrectionLine();

    if (!imageData || imageData.length / 4 / width != height) {
        imageData = ctx.getImageData(0, 0, width, height).data;
    }

    for (let i = 0; i < imageData.length; i+=4) {
        buffer[i] = correctionData[imageData[i]];
        buffer[i + 1] = correctionData[imageData[i + 1]];
        buffer[i + 2] = correctionData[imageData[i + 2]];
        buffer[i + 3] = 255;
    }

    setImageData(buffer, imageData, width, height, isPreview);

    if (isPreview === null) {
        imageData = null;
    }
}
