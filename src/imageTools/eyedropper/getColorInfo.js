import calcContrast from "./calcContrast";
import convertRBGtoXYZ from "./convertRBGtoXYZ";
import convertXYZtoLab from "./convertXYZtoLab";


let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');

let firstColorCoord = document.getElementsByClassName('color_coord')[0];
let firstColorSwatch = document.getElementsByClassName('color_swatch')[0];
let firstColorRGB = document.getElementsByClassName('color_rgb')[0];
let firstColorXYZ = document.getElementsByClassName('color_xyz')[0];
let firstColorLab = document.getElementsByClassName('color_lab')[0];

let secColorCoord = document.getElementsByClassName('color_coord')[1];
let secColorSwatch = document.getElementsByClassName('color_swatch')[1];
let secColorRGB = document.getElementsByClassName('color_rgb')[1];
let secColorXYZ = document.getElementsByClassName('color_xyz')[1];
let secColorLab = document.getElementsByClassName('color_lab')[1];

let contrastLabel = document.getElementsByClassName('contrast_label')[0];
let contrastValue = document.getElementsByClassName('contrast_value')[0];
let contrastAlert = document.getElementsByClassName('contrast_alert')[0];

export default function getColorInfo() {
    let [R1, G1, B1] = [];
    let [R2, G2, B2] = [];

    let fixedColor1 = [];
    let fixedColor2 = [];

    let isFirstColorLocked = false;
    let isSecColorLocked = false;

    canvas.addEventListener('mousemove', e => {
        if (e.ctrlKey) {
            [R2, G2, B2] = ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
            if (!isSecColorLocked) {
                secColorSwatch.style.backgroundColor = `rgb(${R2}, ${G2}, ${B2})`;
            }
        } else {
            [R1, G1, B1] = ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
            if (!isFirstColorLocked) {
                firstColorSwatch.style.backgroundColor = `rgb(${R1}, ${G1}, ${B1})`;
            }
        }
    })

    canvas.addEventListener('mousedown', e => {
        if (e.ctrlKey) {
            [R2, G2, B2] = ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
            fixedColor2 = [R2, G2, B2];

            let [X2, Y2, Z2] = convertRBGtoXYZ([R2, G2, B2]).map((coord) => coord.toFixed(1));
            let [L2, a2, b2] = convertXYZtoLab([X2, Y2, Z2]).map((coord) => coord.toFixed(1));

            secColorCoord.innerText = `x: ${e.offsetX} y: ${e.offsetY} `;
            secColorRGB.innerText = `RGB (${R2}, ${G2}, ${B2})`;
            secColorXYZ.innerText = `XYZ (${X2}, ${Y2}, ${Z2})`;
            secColorLab.innerText = `Lab (${L2}, ${a2}, ${b2})`;
            secColorSwatch.style.backgroundColor = `rgb(${R2}, ${G2}, ${B2})`;
            isSecColorLocked = true;
        } else {
            [R1, G1, B1] = ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
            fixedColor1 = [R1, G1, B1];
            
            let [X1, Y1, Z1] = convertRBGtoXYZ([R1, G1, B1]).map((coord) => coord.toFixed(1));
            let [L1, a1, b1] = convertXYZtoLab([X1, Y1, Z1]).map((coord) => coord.toFixed(1));
            
            firstColorCoord.innerText = `x: ${e.offsetX} y: ${e.offsetY} `;
            firstColorRGB.innerText = `RGB (${R1}, ${G1}, ${B1})`;
            firstColorXYZ.innerText = `XYZ (${X1}, ${Y1}, ${Z1})`;
            firstColorLab.innerText = `Lab (${L1}, ${a1}, ${b1})`;
            firstColorSwatch.style.backgroundColor = `rgb(${R1}, ${G1}, ${B1})`;
            isFirstColorLocked = true;
        }

        if (isFirstColorLocked && isSecColorLocked) {
            let contrast = calcContrast(fixedColor1, fixedColor2).toFixed(1);
            
            if (contrast < 4.5) {
                contrastAlert.style.display = 'block';
                contrastValue.style.color = 'red';
            } else {
                contrastAlert.style.display = 'none';
                contrastValue.style.color = 'black';
            }

            contrastLabel.style.display = 'block';
            contrastValue.innerText = contrast + ':1';
        }
    })    
}
