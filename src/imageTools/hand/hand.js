import move from "./move";

let canvas = document.getElementById("canvas");
let handButton = document.getElementById("hand_button");

export default function hand() {
    handButton.onclick = () => {
        if (handButton.classList.contains('active')) {
            canvas.onmousedown = () => {};
        } else {
            move();
        }
    }
}
