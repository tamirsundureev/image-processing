let canvas = document.getElementById("canvas");
let con = document.getElementsByClassName("con")[0];
let headerHeight = document.getElementsByClassName("header")[0].offsetHeight;

export default function move() {
    canvas.onmousedown = function(e) {
        let coords = getCoords(canvas, con);
        let shiftX = e.pageX - coords.left;
        let shiftY = e.pageY - coords.top;

        canvas.style.marginLeft = '0px';
		canvas.style.marginTop = '0px';
        canvas.style.position = 'relative';
        
        moveAt(e);

        function moveAt(e) {
            canvas.style.left = e.pageX - shiftX + 'px';
            canvas.style.top = e.pageY - headerHeight - shiftY + 'px';
        }

        document.onmousemove = function(e) {
            moveAt(e);
        }
        
        canvas.onmouseup = function() {
            document.onmousemove = null;
            canvas.onmouseup = null;
        }
    }
    
    canvas.ondragstart = function() {
        return false;
    };
    
    function getCoords(elem, con) {
        var box = elem.getBoundingClientRect();
        return {
            top: box.top + con.scrollTop,
            left: box.left + con.scrollLeft,
        };
    }
}
