import createImage from "./createImage";
import drawScaledImage from "../drawScaledImage/drawScaledImage";
import activeTools from "../imageTools/activeTools"


let loadButton = document.getElementById("load_button");
let dialogButton = document.getElementById("dialog_button");
let saveImageButton = document.getElementById("save_button");
let handButton = document.getElementById("hand_button");
let eyedropperButton = document.getElementById("eyedropper_button");
let colorButton = document.getElementById("color_button");
let filterButton = document.getElementById("open_filter_offcanvas_button");

let urlInput = document.getElementById("url_load_input");
let fileInput = document.getElementById("file_load_input");

export default async function loadImage() {
    loadButton.onclick = async () => {
        let image = await createImage({file: fileInput.files[0], url: urlInput.value});

        image.onload = () => {
            drawScaledImage(image);
            activeTools();
        }

        dialogButton.disabled = false;
        saveImageButton.disabled = false;
        eyedropperButton.disabled = false;
        handButton.disabled = false;
        colorButton.disabled = false;
        filterButton.disabled = false;
        
        urlInput.value = null;
        fileInput.value = null;
    };
}
